particlesJS.load('particles-js', 'particlesjs-config.json', function() {
    console.log('particles.js loaded - callback');
});


function validateEmail(sEmail) {
    var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

$(document).ready( function (event) {
   $("#btn-submit").click( function () {
       var sEmail = $("#email").val();
       if ( $.trim(sEmail).length == 0 ) {
           alert('Please enter your email before submitting');
           return false;
       }
       if (validateEmail(sEmail)) {
           $.post("subscribe.php", {"email": sEmail}, function (response) {
                if ( response == "1" ) {
                    $(".step1").hide();
                    $(".step1-form").css("visibility", "hidden");
                    $(".message").slideDown();
                }else if ( response == "3" ){
                    alert('Something went wrong, please try again');
                }else{
                    alert('Please enter a valid email address');
                }
           });
       }
       else {
           alert('Please enter a valid email address');
           return false;
       }
   });
});
